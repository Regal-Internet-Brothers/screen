screen
======

A messy, but functional module for the [Monkey programming language](https://github.com/blitz-research/monkey), which allows you to change the screen/display resolution, and other related functionality. Currently, there is support for the generic display system provided by Mojo, as well as the GLFW2/Desktop specific interface.
